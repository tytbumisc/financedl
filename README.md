A python script for downloading finance data


### To Install

Requires Python 3 and Pip 3

`git clone https://gitlab.com/tytbumisc/financedl`

`cd financedl`

`pip install -r requirements.txt`

`python financeDl.py`

