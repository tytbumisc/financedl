import yfinance as yf
from openpyxl import Workbook
import pandas as pd


# Readability? Where we're going, we won't need readability.

print("Enter ticker symbols, separated by spaces")
symbols = input()
symbols = symbols.strip()
intervals = ["3mo", "1mo", "1wk", "1d", "1h", "30m", "15m", "5m", "2m", "1m"]
timeStrings = ["%Y-%m", "%Y-%m", "%Y-%m-%d", "%Y-%m-%d", "%Y-%m-%d %H:%M:%S",
               "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S"]
dayPeriods = ["1d", "5d", "1mo"]
periods = ["1d", "5d", "1mo", "3mo", "6mo", "1y", "2y", "5y", "10y", "ytd", "max"]
finish = False
while not finish:
    while True:
        print("Choose an interval for the data\n1: 3 months\n2: 1 month\n3: 1 week")
        print("4: 1 day\n5: 1 hour\n6: 30m")
        # print("4:1 day\n5: 1 hour\n6: 30m\n7: 15m\n8: 5m\n9: 2m\n10: 1m")
        choice = input()
        choice = choice.strip()
        try:
            choice = int(choice) - 1
        except ValueError:
            print("Invalid input")
            continue
        try:
            interval = intervals[choice]
            timeString = timeStrings[choice]
        except IndexError:
            print("Invalid input")
            continue
        break

    if choice > 4:
        periodUnit = ""
        while True:
            print("How many days back do you want the data\n1: 1 day\n2: 5 days\n3: 1 month")
            choice = input()
            choice = choice.strip()
            try:
                choice = int(choice) - 1
            except ValueError:
                print("Invalid input")
                continue
            try:
                period = dayPeriods[choice]
            except IndexError:
                print("Invalid input")
                continue
            break
    else:
        while True:
            print("How far back do you want data for\n1: 1 day\n2: 5 days\n3: 1 month")
            print("4: 3 months\n5: 6 months\n6: 1 year\n7: 2 years\n8: 5 years\n9: 10 years\n10: YTD\n11:max")
            choice = input()
            choice = choice.strip()
            try:
                choice = int(choice) - 1
            except ValueError:
                print("Invalid input")
                continue
            try:
                period = periods[choice]
            except IndexError:
                print("Invalid input")
                continue
            break
    toSave = input("Enter the name of the file to save (Saves in the directory the script was run from): ")
    toSave = toSave.replace(' ', '_')
    while True:
        print("Write the past <" + period + "> for every <" + interval +
              "> for symbols <" + symbols + "> to file <" + toSave + ".xlsx>?(y/n)")
        response = input().lower()
        if response == 'y' or response == "yes":
            finish = True
            break
        elif response == 'n' or response == "no":
            break
        else:
            print("Invalid response")
            continue
wb = Workbook()
ws = wb.create_sheet(symbols)
tk = yf.Tickers(symbols)
keys = list(tk.tickers)
# print(tk.history(interval=interval, period=period).to_string())

dataframe = pd.DataFrame()
for j in range(len(keys)):
    hist = tk.tickers[keys[j]].history(interval=interval, period=period)
    for i in range(hist.index.size):
        if not pd.isna(hist.iloc[i]["Open"]): # don't add rows where the value is NaN
            ser = pd.Series(data = [keys[j]], index = ["Ticker"]) # the ticker name, added to the end of each series in the frame
            ser2 = hist.iloc[i].append(ser) # concat the stock data and ticker name
            ser2.name = hist.index[i]
            dataframe = dataframe.append(ser2)



# dataframe has all of the data from individual tickers as well as the associated ticker names
# the dataframe contains all data from the tickers in sequence by date/time, so to iterate through the
# data of all tickers, iterate through the dataframe in one dimension.
dataframe = dataframe.sort_index()
print(dataframe)

ws.cell(row = 1, column = 1, value = "Date/Time")
for i in range(len(keys)):
    ws.cell(row=1, column=2 + i, value=keys[i])  # populates first row with the names of the tickers

dataSize = 1 # the total number of rows of data, or the number of unique timestamps
for i in range(dataframe.index.size): # iterates through data and sets cells in correct columns to values.
    if ws.cell(row = 2 + dataSize, column = 1).value != dataframe.index[i].strftime(timeString): # current time does not exist yet, create new row
        dataSize += 1
        ws.cell(row=2 + dataSize, column=1, value=dataframe.index[i].strftime(timeString))
        ws.cell(row=2 + dataSize, column=keys.index(dataframe["Ticker"][i]) + 2, value=dataframe["Open"][i])
    else: # current time does exist, use it
        ws.cell(row=2 + dataSize, column=keys.index(dataframe["Ticker"][i]) + 2, value=dataframe["Open"][i])

alphabet = list("abcdefghijklmnopqrstuvwxyz")


# percent change
numberOfTicks = len(keys)
ws.cell(row=1, column=2+numberOfTicks, value="Date/Time")

for i in range(numberOfTicks):
    ws.cell(row=1, column= 3 + i + numberOfTicks, value=keys[i] + "\u0394%")

for i in range(dataSize):
    ws.cell(row=4+i, column=2+numberOfTicks, value=None if ws.cell(row=4+i, column=1).value == None else "=A"+str(4+i))

colInd = 0
for col in ws.iter_cols(min_row = 4, min_col = 3 + numberOfTicks, max_row = 4 + dataSize, max_col = 2 + 2*numberOfTicks): 
    colInd += 1
    baseCell = "$" + alphabet[colInd] + "$" + "4"
    for rowInd in range(len(col)):
        if (ws.cell(row=rowInd+4, column=colInd).value == None): #if the source cell has no value, the output cell has no value
            ws.cell(row=rowInd+4, column=colInd+numberOfTicks+2, value = None)
            continue
        exprStr = "=(" + alphabet[colInd] + str(rowInd + 4) + "/" + baseCell + "-1)*100" 
        ws.cell(row=rowInd + 4, column=colInd+numberOfTicks + 2, value=exprStr)


    


wb.save(toSave + ".xlsx")

"""
for j in range(len(keys)):
    tk2 = tk.tickers[keys[j]]
    ws.cell(row = 1, column = 1 + j * 2, value = tk2.get_info()["symbol"])
    hist = tk2.history(interval=interval, period=period)
    for i in range(hist.index.size):
        ws.cell(row = i + 2, column = 1 + j * 2, value = hist.index[i].strftime(timeString))
        ws.cell(row = i + 2, column = 2 + j * 2, value = hist["Open"][i])
"""
